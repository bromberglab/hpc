import argparse
import configparser
import csv
import os
import re
import time

from bblsuite.helper import config, logger, mysql
from bblsuite.hpc.cluster import Cluster
from bblsuite.hpc.states import JobState, DaemonState

"""Validate finished jobs"""

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class ProjectValidator:

    OUTPUT_DIR = config.Config().get_property('clubber', 'remote.output.dir')

    def __init__(self, project_name=None, db=None):
        self.log = logger.Logger.get_logger()
        self.project_name = project_name
        self.db = db

    @staticmethod
    def __read_validation_file(file):
        """Read part validation results from file"""

        validated = {}
        with open(file, 'r') as csv_in:
            file_in = csv.reader(csv_in, delimiter='\t')
            for row in file_in:
                validated[int(row[0])] = row[1]
        return validated

    @staticmethod
    def __read_dirlist_file(file):
        """Read listed output directories from file"""

        validated = {}
        pattern = r'.*/([0-9]+\.[0-9]+)$'
        with open(file, 'r') as file_in:
            for line in file_in:
                matched = re.match(pattern, line)
                if matched is not None:
                    matches = matched.group(1).split(".")
                    jobid = int(matches[0])
                    # arrayidx = int(matches[1])
                    if jobid in validated:
                        validated[jobid] += 1
                    else:
                        validated[jobid] = 1
        return validated

    def validate_project_jobs_results_listing(self, cluster_name, file):
        """Get list of jobs from database and check against file containing list of result folders"""

        jobs_ok = 0
        validation_results = dict()
        validated = ProjectValidator.__read_dirlist_file(file)
        cluster = Cluster(cluster_name)

        project = ''
        if self.project_name is not None:
            project = 'AND project="%s"' % self.project_name
        jobid_list = self.db.query(
            'SELECT jobid, COUNT(jobid) AS jobcount FROM jobs WHERE cluster="%s" %s AND status=%i '
            'GROUP BY jobid' %
            (
                cluster.name, project, JobState.FINISHED.value
            )
        )

        for jobid, jobcount in jobid_list:
            jobid = int(jobid)
            if jobid in validated:
                resultcount = validated[jobid]
                if resultcount != jobcount:
                    validation_results[jobid] = "%i/%i" % (resultcount, jobcount)
                else:
                    validation_results[jobid] = "ok"
                    jobs_ok += 1
            else:
                validation_results[jobid] = "%i/%i" % (0, jobcount)

        print("%i of %s jobs successfully validated" % (jobs_ok, len(jobid_list)))

        return validation_results

    def check_finished_jobs(self, jobs, cluster):
        """Check if jobs are finished and not in the cluster submission system any more, returns db ids from finished"""

        running_jobs = []
        submitted_jobs = {}
        for registered_job in jobs:
            registered_full_job_id = str(registered_job.job_id)
            if registered_job.job_array_idx is not 'NULL':
                registered_full_job_id += '%s%s' % (cluster.load_manager.jobid_seperator, registered_job.job_array_idx)
            submitted_jobs[registered_full_job_id] = registered_job

        cluster_jobs = cluster.get_all_jobs()
        for still_active_job in cluster_jobs:
            still_active_full_job_id = str(still_active_job.job_id)
            if still_active_job.job_array_idx is not None:
                still_active_full_job_id += '%s%s' % (
                    cluster.load_manager.jobid_seperator, still_active_job.job_array_idx
                )
                running_job = submitted_jobs.pop(still_active_full_job_id, None)
                if running_job:
                    running_jobs.append(running_job)
            else:
                pending_details = cluster.load_manager.get_pending_info(still_active_job)
                if pending_details[0] == 1:
                    running_job = submitted_jobs.pop(still_active_full_job_id, None)
                    if running_job:
                        running_jobs.append(running_job)
                else:
                    for idx in range(pending_details[1][0], pending_details[1][1]+1):
                        array_job_id = '%s%s%s' % (still_active_full_job_id, cluster.load_manager.jobid_seperator, idx)
                        running_job = submitted_jobs.pop(array_job_id, None)
                        if running_job:
                            running_jobs.append(running_job)

        for finished_job in submitted_jobs.values():
            finished_job.status = JobState.FINISHED.value
            finished_job.daemon = DaemonState.VALIDATED.value

        for running_job in running_jobs:
            running_job.status = JobState.RUNNING.value
            running_job.daemon = DaemonState.SUBMITTED.value

        if len(submitted_jobs) > 0:
            self.log.info('[%s] %i jobs finished' % (cluster.name, len(submitted_jobs)))
        else:
            self.log.debug('[%s] %i jobs finished' % (cluster.name, len(submitted_jobs)))

        return list(submitted_jobs.values()), running_jobs

    @staticmethod
    def check_finished_job_info(jobs, cluster, project):
        """ Retrieve job_info files of jobs"""

        finished_ok = []
        finished_failed = []

        array_jobs = []
        array_job_ids = set()
        single_jobs = []
        for job in jobs:
            if job.is_array_job:
                array_jobs.append(job)
                array_job_ids.add(job.job_id)
            else:
                single_jobs.append(job)

        array_infos = []
        for array_job_id in array_job_ids:
            array_infos.append(cluster.retrieve_array_job_info_files(project, array_job_id))
        single_infos = []
        for job in single_jobs:
            single_infos.append(cluster.retrieve_job_info_files(project, job.job_id))

        parser = configparser.ConfigParser()
        for a_info in array_infos:
            parser.read_string(a_info)
        for s_info in single_infos:
            parser.read_string(s_info)

        for job in jobs:
            job_id = job.get_cluster_job_output_id()
            if parser.has_option(job_id, "end"):
                start = int(parser.get(job_id, "start"))
                end = int(parser.get(job_id, "end"))
                # datetime.datetime.fromtimestamp(int(start)).strftime('%Y-%m-%d %H:%M:%S')
                job.start = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start))
                job.end = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(end))
                finished_ok.append(job)
            else:
                finished_failed.append(job)

        return finished_ok, finished_failed

    def validate_job_result(self, cluster, job):
        """ Check result files for single jobs"""

        return self.validate_jobs_results(cluster, [job])

    def validate_jobs_results(self, cluster, jobs):
        """ Check result files for multiple jobs"""

        seperator = cluster.load_manager.output_seperator

        for job in jobs:
            jobarray = ""
            if job.job_array_idx is not None:
                jobarray = "%s*" % seperator
            cmd = 'ls -1Ud %s/%s/%s/%i%s | wc -l' % (
                cluster.basedir, self.project_name, self.OUTPUT_DIR, job.job_id, jobarray
            )
            output = cluster.ssh.send_shell(cmd)

            try:
                resultcount = int(output)
                if resultcount == 1:
                    return True
                else:
                    return False
            except ValueError:
                return False

    @staticmethod
    def check_arrayidx(jobid, job_count, resultdir_list):
        """Validate sub jobs within an array job"""

        missing_arrayidx = []

        for idx in range(1, job_count+1):
            job_dir = '%i.%i' % (jobid, idx)
            if job_dir not in resultdir_list:
                missing_arrayidx.append(idx)

        return missing_arrayidx

    def validate_project_jobs(self, cluster_name):
        """Get list of jobs from database and check for result folders on clusters using ls"""

        cluster = Cluster(cluster_name)

        project = ''
        if self.project_name is not None:
            project = 'AND project="%s"' % self.project_name
        # get array job ids with counts
        jobid_list = self.db.query(
            'SELECT jobid, COUNT(jobid) AS jobcount FROM jobs WHERE cluster="%s" %s AND status=%i '
            'GROUP BY jobid' %
            (
                cluster_name, project, JobState.FINISHED.value
            )
        )

        registered_jobs_count = len(jobid_list)
        validated_count = 0
        validation_errors = dict()
        seperator = cluster.load_manager.output_seperator
        cluster.connect()

        if registered_jobs_count == 0:
            self.log.info('All jobs of project [%s] already validated.' % self.project_name)
            return validation_errors

        self.log.info('Validating job [%i] (job count: %i) on %s' % (
              int(jobid_list[0]), registered_jobs_count, cluster.name
            )
        )
        for jobid, job_count in jobid_list:
            jobid = int(jobid)
            job_count = int(job_count)
            #  preval = False
            jobarray = ""
            if job_count > 1:
                jobarray = "%s*" % seperator

            cmd = 'ls -1Ud %s/%s/%s/%i%s | xargs -n1 basename' % (
                cluster.basedir, self.project_name, self.OUTPUT_DIR, jobid, jobarray
            )
            resultdir_out = cluster.ssh.send_shell(cmd)

            if resultdir_out.startswith('ls: '):
                validation_errors[jobid] = (0, job_count)
            else:
                resultdir_list = resultdir_out.split('\n')
                resultdir_count = len(resultdir_list)
                if resultdir_count != job_count:
                    missing = ProjectValidator.check_arrayidx(jobid, job_count, resultdir_list)
                    validation_errors[jobid] = (resultdir_count, job_count, missing)
                else:
                    validated_count += 1

        cluster.disconnect()

        self.log.info("Validated: %i/%i" % (validated_count, registered_jobs_count))

        return validation_errors

    def validate_project_jobs_tsv(self, cluster_name, validation_file=None):
        """Get list of jobs from database and check for result folders on clusters using ls, export to tsv"""

        pre_validated = {}
        if validation_file is None:
            validation_file = '%s_validation.csv' % cluster_name
        if os.path.isfile(validation_file):
            pre_validated = ProjectValidator.__read_validation_file(validation_file)

        cluster = Cluster(cluster_name)

        project = ''
        if self.project_name is not None:
            project = 'AND project="%s"' % self.project_name
        jobid_list = self.db.query('SELECT jobid, COUNT(jobid) AS jobcount FROM jobs '
                                   'WHERE cluster="%s" %s AND status=%i GROUP BY jobid' % (
                                       cluster_name, project, JobState.FINISHED.value
                                   ))

        job_count_registered = len(jobid_list)
        jobs_ok = 0
        validation_results = dict()
        seperator = cluster.load_manager.output_seperator
        cluster.connect()

        print('Validating %s jobs on %s' % (job_count_registered, cluster.name))
        for jobid, jobcount in jobid_list:
            jobid = int(jobid)
            preval = False
            jobarray = ""
            if jobcount > 1:
                jobarray = "%s*" % seperator
            if jobid not in pre_validated:
                cmd = 'ls -1Ud %s/%s/%s/%i%s | wc -l' % (
                    cluster.basedir, self.project_name, self.OUTPUT_DIR, jobid, jobarray
                )
                output = cluster.ssh.send_shell(cmd)
            else:
                preval = True
                output = pre_validated[jobid].split("/")[0]
            try:
                resultcount = int(output)
                res = "%i\t%s/%s" % (jobid, resultcount, jobcount)
                print(res)
                if not preval:
                    with open(validation_file, 'a') as out:
                        out.write("%s\n" % res)
                if resultcount != jobcount:
                    validation_results[jobid] = "%i/%i" % (resultcount, jobcount)
                else:
                    validation_results[jobid] = "ok"
                    jobs_ok += 1
            except ValueError:
                print("%i has no result(s)" % jobid)

        cluster.disconnect()

        print("%i of %s jobs successfully validated" % (jobs_ok, job_count_registered))

        return validation_results


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Validate jobs submitted by manager')
    parser.add_argument('-p', '--project', metavar='project_name',
                        help='Name of project')
    parser.add_argument('-c', '--cluster', metavar='cluster1 [-c cluster2]', action='append',
                        help='Validate jobs run at given clusters')
    parser.add_argument('-f', '--file', metavar='validated_jobs.csv',
                        help='File with validated jobs from an earlier run')
    parser.add_argument('-l', '--list', metavar='list.txt', default=None,
                        help='File with list of output directories')
    return parser


def main(arguments):
    proj = arguments.project
    clusters = arguments.cluster
    file = arguments.file
    args_list = arguments.list
    db = mysql.MySQLHandler('hpc')
    validator = ProjectValidator(proj, db)
    if args_list is not None:
        for cluster in clusters:
            validator.validate_project_jobs_results_listing(cluster, args_list)
    else:
        for cluster in clusters:
            validator.validate_project_jobs_tsv(cluster, file)


if __name__ == "__main__":
    args = get_argument_parser().parse_args()
    main(args)
